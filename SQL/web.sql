﻿CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;
USE usermanagement;

CREATE TABLE user (
    id SERIAL primary key AUTO_INCREMENT,
    login_id varchar(255) UNIQUE Not Null, 
    name varchar(255)Not Null, 
    birth_date DATE Not Null,
    password varchar(255) Not Null, 
    create_date DATETIME Not Null, 
    update_date DATETIME Not Null
);

INSERT INTO user(login_id,name,birth_date,password,create_date,update_date) VALUES(
    'admin',
    '管理者',
    '1997-01-01',
    'aiueo',
    NOW(),
    NOW()
);

SELECT * FROM user WHERE login_id = ? and password = ?;

UPDATE user SET name = 'かんりしゃ' WHERE id = 1;

INSERT INTO user VALUES(1, 'admin', '管理者', '1997-01-01', 'a', NOW(), NOW());


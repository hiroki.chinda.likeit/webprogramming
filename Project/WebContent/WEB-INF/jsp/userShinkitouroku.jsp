<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ新規登録</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
    <style type="text/css">
        .kakunin{
            display: inline-block;
            margin:5px;
            position:relative;
            left:1000px;
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-light" style="background-color: #808080;">
            <p class="kakunin">${userInfo.name}さん</p>
            <a href="loginServlet">ログアウト</a>
    </nav>

    <h1>ユーザ新規登録</h1>

    <c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>


    <form action="UserRegistration" method="post">
      <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">ログインID</label>
        <div class="col-sm-5" >
          <input type="text" class="form-control" id="inputEmail3" name="loginId">
        </div>
      </div>

      <div class="form-group row">
        <label for="inputPassword3" class="col-sm-2 col-form-label">パスワード</label>
        <div class="col-sm-5">
          <input type="password" class="form-control" value="${user.password}" name="password">
        </div>
      </div>

      <div class="form-group row">
        <label for="inputPassword3" class="col-sm-2 col-form-label">パスワード（確認）</label>
        <div class="col-sm-5">
          <input type="password" class="form-control" value="${user.password}" name="password2">
        </div>
      </div>

      <div class="form-group row">
        <label for="inputPassword3" class="col-sm-2 col-form-label">ユーザ名</label>
        <div class="col-sm-5">
          <input type="text" class="form-control" id="inputPassword3" name="name">
        </div>
      </div>

      <div class="form-group row">
        <label for="inputPassword3" class="col-sm-2 col-form-label">生年月日</label>
        <div class="col-sm-5">
          <input type="date" class="form-control" id="inputPassword3" name="birthDate">
        </div>
      </div>

      <div class="form-group row">
        <div class="col-sm-10">
          <button type="submit" class="btn btn-primary" >登録</button>
        </div>
      </div>
    </form>



    <a href="UserListServlet">戻る</a>

</body>
</html>

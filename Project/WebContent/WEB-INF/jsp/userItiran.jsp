<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ一覧</title>
    <link href="style3.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
    <style type="text/css">
        .kakunin{
            display: inline-block;
            margin:5px;
            position:relative;
            left:1000px;
        }

        table{
            width:600px;
            height:10px;
            }
    </style>
</head>
<body>
    <nav class="navbar navbar-light" style="background-color: #808080;">
            <p class="kakunin">${userInfo.name}さん</p>
            <a href="loginServlet">ログアウト</a>
    </nav>

    <h1>ユーザ一覧</h1>


        <a href="UserRegistration">新規登録</a>




        <form action="UserListServlet" method="post">
          <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">ログインID</label>
            <div class="col-sm-5">
              <input type="text" class="form-control" value="${user.loginId}" name="loginId" id="inputEmail3">
            </div>
          </div>

          <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">ユーザ名</label>
            <div class="col-sm-5">
              <input type="text" class="form-control" value="${user.name}" name="name" id="inputPassword3">
            </div>
          </div>

          <div class="form-group row">
            <label for="inputPassword3" class="col-sm-2 col-form-label">生年月日</label>
            <div class="col-sm-2">
              <input type="date" class="form-control" value="${user.birthDate}" name="birthDate" id="inputPassword3">
            </div>
              ~
            <div class="col-sm-2">
              <input type="date" class="form-control" value="${user.birthDate}" name="birthDate2" id="inputPassword3">
            </div>
          </div>


          <div class="form-group row">
            <div class="col-sm-10">
              <button type="submit" class="btn btn-primary">検索</button>
            </div>
          </div>
        </form>






<hr width="90%">

    <table class="table table-striped">
        <tr style="text-align: center;">
            <th>ログインID</th>
            <th>ユーザ名</th>
            <th>生年月日</th>
            <th></th>
        </tr>
         <c:forEach var="user" items="${userList}" >
           <tr style="text-align: center;">
             <td>${user.loginId}</td>
             <td>${user.name}</td>
             <td>${user.birthDate}</td>
             <!-- TODO 未実装；ログインボタンの表示制御を行う -->

             <td>
	              <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
	            <c:if test="${userInfo.loginId == 'admin' or user.loginId == userInfo.loginId}">
	              <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
	            </c:if>
	            <c:if test="${userInfo.loginId == 'admin'}">
	              <a class="btn btn-danger" href ="UserDeleteServlet?id=${user.id}">削除</a>
	            </c:if>
             </td>
           </tr>
         </c:forEach>


    </table>

	</body>
</html>

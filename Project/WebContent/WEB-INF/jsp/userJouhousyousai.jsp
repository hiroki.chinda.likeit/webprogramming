<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ情報詳細参照</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
    <style type="text/css">
        .kakunin{
            display: inline-block;
            margin:5px;
            position:relative;
            left:1000px;
        }
        table{
            font-size:20px;
            height: 500px;
        }

        th{
            padding:10px 500px 10px 100px;
            }

    </style>
</head>
	<body>
    <nav class="navbar navbar-light" style="background-color: #808080;">
            <p class="kakunin">${userInfo.name}さん</p>
            <a href="loginServlet">ログアウト</a>
    </nav>

    <h1>ユーザ情報詳細参照</h1>

    <table  align="center">
        <tr>
            <th>ログインID</th>
            <td>${user.loginId}</td>
        </tr>
        <tr>
            <th>ユーザ名</th>
            <td>${user.name}</td>
        </tr>
        <tr>
            <th>生年月日</th>
            <td>${user.birthDate}</td>
        </tr>
        <tr>
            <th>登録日時</th>
            <td>${user.createDate}</td>
        </tr>
        <tr>
            <th>更新日時</th>
            <td>${user.createDate}</td>
        </tr>

    </table>
    <a href="UserListServlet">戻る</a>
	</body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ削除確認</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
    <style type="text/css">
        .kakunin{
            display: inline-block;
            margin:5px;
            position:relative;
            left:1000px;
        }

        .kakunin2{
            font-size: 20px;
            padding:10px 10px 10px 600px;

        }

        .kakunin3{

        }


    </style>
</head>
<body>
    <nav class="navbar navbar-light" style="background-color: #808080;">
            <p class="kakunin">${userInfo.name}さん</p>
            <a href="loginServlet">ログアウト</a>
    </nav>

    <h1>ユーザ削除確認</h1>
    <div class="kakunin2">
        <p>ログインID : ${user.loginId}<br>
           を本当に削除してよろしいでしょうか。
        </p>
    </div>

    <div class="kakunin3" align="center">
	    <form action="UserListServlet" method="get">
	     	<button type="submit" class="btn btn-primary" style="margin: 10px 40px 10px 10px;">キャンセル</button>
	    </form>
    	<form action="UserDeleteServlet" method="post">
    	  <input type="hidden" value="${user.loginId}" name="loginId">
	        <button type="submit" class="btn btn-primary" style="margin: 10px 10px 10px 50px;">OK</button>
        </form>
    </div>

</body>
</html>

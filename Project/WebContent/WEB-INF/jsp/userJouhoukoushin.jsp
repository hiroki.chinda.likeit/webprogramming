<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ情報更新</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">

    <style type="text/css">
        .kakunin{
            display: inline-block;
            margin:5px;
            position:relative;
            left:1000px;
        }
        form{
            font-size:20px;
            height: 500px;
            padding:10px 10px 10px 300px;
        }


    </style>

</head>
<body>

    <nav class="navbar navbar-light" style="background-color: #808080;">
            <p class="kakunin">${userInfo.name}さん</p>
            <a href="loginServlet">ログアウト</a>
    </nav>

    <h1>ユーザ情報更新</h1>



     <c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>

    <form action="UserUpdateServlet" method="post">
      <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">ログインID</label>
        <div class="col-sm-5">
          <p style="margin-left: 250px;">${user.loginId}</p>
          <input type="hidden" value="${user.id}" name="id">
        </div>

      </div>

      <div class="form-group row">
        <label for="inputPassword3" class="col-sm-2 col-form-label">パスワード</label>
        <div class="col-sm-5">
          <input type="password" class="form-control" value="${user.password}" name="password">
        </div>
      </div>

      <div class="form-group row">
        <label for="inputPassword3" class="col-sm-2 col-form-label">パスワード（確認）</label>
        <div class="col-sm-5">
          <input type="password" class="form-control" value="${user.password}" name="password2">
        </div>
      </div>

      <div class="form-group row">
        <label for="inputPassword3" class="col-sm-2 col-form-label">ユーザ名</label>
        <div class="col-sm-5">
          <input type="text" class="form-control" value="${user.name}" name="name" >
        </div>
      </div>

      <div class="form-group row">
        <label for="inputPassword3" class="col-sm-2 col-form-label">生年月日</label>
        <div class="col-sm-5">
          <input type="date" class="form-control" value="${user.birthDate}" name="birthDate">
        </div>
      </div>

      <div class="form-group row">
        <div class="col-sm-10">
          <button type="submit" class="btn btn-primary">登録</button>
        </div>
      </div>
    </form>

    <a href="UserListServlet">戻る</a>


</body>
</html>

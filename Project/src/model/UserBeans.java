package model;

import java.io.Serializable;
import java.util.Date;

public class UserBeans implements Serializable{
	private int id;
	private String loginId;
	private String name;
	private Date birthDate;
	private String password;
	private String createDate;
	private String updateDate;

	// ログインセッションを保存するためのコンストラクタ
	public UserBeans(String loginId, String name) {
		this.loginId = loginId;
		this.name = name;
	}


	// 全てのデータをセットするコンストラクタ
	public UserBeans(int id, String loginId, String name, Date birthDate, String password, String createDate,
			String updateDate) {
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.password = password;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}

	public UserBeans(int id) {
		this.id = id;
	}
	//ユーザ参照のデータをセットするコンストラクタ
	public UserBeans(String loginId, String name, Date birthDate, String createDate, String updateDate) {
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}

	//ユーザ参照のデータをセットするコンストラクタ
	public UserBeans(int id,String loginId, String name, Date birthDate, String createDate, String updateDate) {
		this.id=id;
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}

	// ユーザ更新（パスワードあり）をセットするコンストラクタ
	public UserBeans(String name, Date birthDate, String password, int id) {
		this.name = name;
		this.birthDate = birthDate;
		this.password = password;
		this.id = id;
	}

	// ユーザ更新（パスワードなし）をセットするコンストラクタ
		public UserBeans(String name, Date birthDate, int id) {
			this.name = name;
			this.birthDate = birthDate;
			this.id = id;
		}
	//ユーザ削除のデータをセットするコンストラクタ
	public UserBeans(String loginId) {
		this.loginId = loginId;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
}

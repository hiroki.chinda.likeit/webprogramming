

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.UserBeans;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO 未実装：ログインセッションがない場合、ログイン画面にリダイレクトさせる

		//セッションにユーザの情報をセット
		HttpSession session = request.getSession();
		UserBeans userB = (UserBeans) session.getAttribute("userInfo");

		if(userB == null) {
			response.sendRedirect("loginServlet");
		}

		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		// TODO  未実装：idを引数にして、idに紐づくユーザ情報を出力する
		UserDao userDao = new UserDao();
		UserBeans user = userDao.getUser(id);

		request.setAttribute("user", user);
		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userJouhoukoushin.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String id = request.getParameter("id");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");



		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		UserDao userDao = new UserDao();
		UserBeans user = userDao.getUser(id);


		/** テーブルに該当のデータが見つからなかった場合 **/


		//パスワードとパスワード（確認）の入力内容が異なる場合
		if(!(password.equals(password2))) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			request.setAttribute("user", user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userJouhoukoushin.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//入力項目に1つでも未入力のものがある場合
		if(/*password.equals("") || password2.equals("") || */ name.equals("") || birthDate.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			request.setAttribute("user", user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userJouhoukoushin.jsp");
			dispatcher.forward(request,  response);
			return;
		};

		//パスワードが入力された場合とそうでない場合
		if(password.equals("")) {
			userDao.userInfoUpdate2(name, birthDate,id);
		}else {
			userDao.userInfoUpdate(password, name, birthDate, id);
		}



		// ユーザ一覧のサーブレットにリダイレクト
		response.sendRedirect("UserListServlet");
	}



}

package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.UserBeans;

public class UserDao {
	public UserBeans findByLoginInfo(String loginId, String password) {
		Connection conn = null;
			try {
				//データベースへ接続
				conn = DBManager.getConnection();

				//処理
				//確認済のSQL
				String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

				//SELECTを実行し、結果表を取得
				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, loginId);
				pStmt.setString(2, password);
				ResultSet rs = pStmt.executeQuery();

				//ログイン失敗時
				if(!rs.next()) {
					return null;
				}

				//ログイン成功時
				String loginIdData = rs.getString("login_id");
				String nameData = rs.getString("name");
				return new UserBeans(loginIdData, nameData);

			}catch(SQLException e) {
				e.printStackTrace();
				return null;
			}finally {
				//データベース切断
				if(conn != null) {
					try {
						conn.close();
					}catch(SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
	}


	/**
     * 全てのユーザ情報を取得する
     * @return
     */
    public List<UserBeans> findAll() {
        Connection conn = null;
        List<UserBeans> userList = new ArrayList<UserBeans>();

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            // TODO: 未実装：管理者以外を取得するようSQLを変更する
            String sql = "SELECT * FROM user WHERE id != 1";

             // SELECTを実行し、結果表を取得
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            // 結果表に格納されたレコードの内容を
            // Userインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                Date birthDate = rs.getDate("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                UserBeans user = new UserBeans(id, loginId, name, birthDate, password, createDate, updateDate);

                userList.add(user);

//                if(!(id == 1)){
//                    userList.add(user);
//                };
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return userList;
    }

    //新規登録
    public void addAll(String loginId, String password, String name, String birthDate){
        Connection conn = null;

        try {
        	//データベースへ接続
        	conn = DBManager.getConnection();

        	String result = this.password(password);

        	 String sql = "INSERT INTO user (login_id, password, name, birth_date,create_date,update_date) VALUES (?, ?, ?, ?,NOW(),NOW())";

        	 PreparedStatement pStmt = conn.prepareStatement(sql);

        	 pStmt.setString(1, loginId);
        	 pStmt.setString(2, result);
        	 pStmt.setString(3, name);
        	 pStmt.setString(4, birthDate);

        	 pStmt.executeUpdate();




        }catch (SQLException e) {
            e.printStackTrace();

        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();

                }
            }
        }


    }
    //新規登録エラー処理
    public UserBeans idVerification(String loginId) {
    	Connection conn = null;

        try {
        	//データベースへ接続
        	conn = DBManager.getConnection();

        	 String sql = "SELECT * FROM user WHERE login_id = ?";

        	 PreparedStatement pStmt = conn.prepareStatement(sql);

        	 pStmt.setString(1, loginId);

        	 ResultSet rs = pStmt.executeQuery();

			//ログイン失敗時
			if(!rs.next()) {
				return null;
			}

			//ログイン成功時
			String loginIdData = rs.getString("login_id");
			return new UserBeans(loginIdData);




        }catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;

                }
            }
        }


    }

    // 更新メソッド（パスワードあり）
    public void userInfoUpdate(String password, String name, String birthDate, String id) {
    	Connection conn = null;

        try {
        	//データベースへ接続
        	conn = DBManager.getConnection();
        	//暗号化
        	String result = this.password(password);

        	 String sql = "UPDATE user SET password = ?, name = ?, birth_date = ? WHERE id = ?";

        	 PreparedStatement pStmt = conn.prepareStatement(sql);

        	 pStmt.setString(1, result);
        	 pStmt.setString(2, name);
        	 pStmt.setString(3, birthDate);
        	 pStmt.setString(4, id);

        	 pStmt.executeUpdate();




        }catch (SQLException e) {
            e.printStackTrace();

        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();

                }
            }
        }
    }

    // 更新メソッド（パスワードなし）
    public void userInfoUpdate2(String name, String birthDate, String id) {
    	Connection conn = null;

        try {
        	//データベースへ接続
        	conn = DBManager.getConnection();

        	 String sql = "UPDATE user SET name = ?, birth_date = ? WHERE id = ?";

        	 PreparedStatement pStmt = conn.prepareStatement(sql);

        	 pStmt.setString(1, name);
        	 pStmt.setString(2, birthDate);
        	 pStmt.setString(3, id);

        	 int result = pStmt.executeUpdate();

        }catch (SQLException e) {
            e.printStackTrace();

        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();

                }
            }
        }
    }

    //削除
    public void userInfoDelete(String loginId) {
    	Connection conn = null;

        try {
        	//データベースへ接続
        	conn = DBManager.getConnection();

        	 String sql = "DELETE FROM user WHERE login_id = ?";

        	 PreparedStatement pStmt = conn.prepareStatement(sql);

        	 pStmt.setString(1, loginId);

        	 int result = pStmt.executeUpdate();




        }catch (SQLException e) {
            e.printStackTrace();

        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();

                }
            }
        }
    }

    //情報詳細、セッション確認用？
    public UserBeans getUser(String id) {
    	Connection conn = null;

    	try {
    		conn = DBManager.getConnection();
    		String sql = "SELECT * FROM user WHERE id = ?";
    		PreparedStatement pStmt = conn.prepareStatement(sql);
    		pStmt.setString(1, id);
    		ResultSet rs = pStmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

			int idData = rs.getInt("id");
    		String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			Date birthDateData = rs.getDate("birth_date");
			String createDateData = rs.getString("create_date");
			String updateDateData = rs.getString("update_date");



			return new UserBeans(idData,loginIdData, nameData, birthDateData, createDateData, updateDateData);

    	}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			//データベース切断
			if(conn != null) {
				try {
					conn.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}


    }

    //暗号化
    public String password(String password) {
    	Connection conn = null;

        try {
        	String source = password;

        	Charset charset = StandardCharsets.UTF_8;

        	String algorithm = "MD5";

        	byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
        	String result = DatatypeConverter.printHexBinary(bytes);

        	System.out.println(result);

        	return result;




        }catch (Exception e) {
            e.printStackTrace();
            return null;

        }


    }

    //ユーザ一覧検索機能

    public List<UserBeans> search(String userId, String userName, String userBirthDate, String userBirthDate2) {
        Connection conn = null;
        List<UserBeans> userList = new ArrayList<UserBeans>();

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            // 検索機能
            String sql = "SELECT * FROM user WHERE id != 1";

            if(!userId.equals("")) {
            		sql+= " AND login_id = '"+userId + "'";
            }

            if(!userName.equals("")) {
            		sql+= " AND name LIKE '%"+userName + "%'";
            }

            if(!userBirthDate.equals("")) {
            		sql+= " AND birth_date >= '"+userBirthDate + "'";
            }

            if(!userBirthDate2.equals("")){
            		sql+=" AND birth_date <= '"+userBirthDate2 + "'";
            }



             // SELECTを実行し、結果表を取得
            Statement stmt = conn.createStatement();

            ResultSet rs = stmt.executeQuery(sql);

            // 結果表に格納されたレコードの内容を
            // Userインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                Date birthDate = rs.getDate("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                UserBeans user = new UserBeans(id, loginId, name, birthDate, password, createDate, updateDate);

                userList.add(user);

            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return userList;
    }
}


